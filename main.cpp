//
//  Andre White
//  Awhiteis21@csu.fullerton.edu
//  Postfix Calculator with variables 
//  CS323
//
//  Created by Andre White on 2/28/16.
//  Copyright © 2016 AndreWhite. All rights reserved.
//

#include <iostream>
#include <string.h>
#include <stack>
using namespace std;

int getValue(char variable){                                    //This function will return the value of variable to be added to the stack
    int value=0;
    printf("%s %c %s", "Enter the value of",variable, ":");     //Prompt user for value
    cin>>value;                                                 //get value
    return value;
}
int parseInput(string input)                                    //This function will parse through the input and perform mathematical operation to return result
{
    stack<int> variables;                                       //integer stack for postfix evaluation
    int op1,op2;                                                //operands for calculation
    char curChar;                                               //current Char for parser
    for(int x=0;x<input.length();x++){
        curChar=input[x];
        if(isalpha(curChar)){                                   //if current character is an alpha character then it is a variable.
            variables.push(getValue(curChar));                  //getValue for variable and push it to stack
        }
        switch(curChar){                                        //if char is an operator, operand1 will be at the top of the stack, and operand two will be next. Push result back to stack
            case '+':
                op1=variables.top();
                variables.pop();
                op2=variables.top();
                variables.pop();
                variables.push(op1+op2);
                break;
            case '-':
                op1=variables.top();
                variables.pop();
                op2=variables.top();
                variables.pop();
                variables.push(op1-op2);
                break;
            case '*':
                op1=variables.top();
                variables.pop();
                op2=variables.top();
                variables.pop();
                variables.push(op1*op2);
                break;
            case '/':
                op1=variables.top();
                variables.pop();
                op2=variables.top();
                variables.pop();
                variables.push(op1/op2);
                break;
        }
    }
    return variables.top();                                     //result of the operation is at the top of the stack
}
char prompt(){
    char response;                                              //user response(y/n)
    string input;                                               //user input
    printf("%s", "Enter a postfix expression:");                //prompt to enter expression
    cin>>input;                                                 //get expression
    int value=parseInput(input);                                //value is the result of the expression
    printf("%s %d \n","Final value=",value);                    //output final value
    printf("%s", "Continue?(y/n)");                             //prompt for response
    cin>>response;                                              //get response
    return response;
}

int main(int argc, char** argv){
    while(prompt()=='y'){                                       //program loop
    }
    return 0;
}

